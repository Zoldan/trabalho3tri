/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.model;

/**
 *
 * @author Aluno
 */
public class FormaPagamento {
    
    private String nomeFormaPag;
    private int codigoFormaPag;

    public String getNomeFormaPag() {
        return nomeFormaPag;
    }

    public void setNomeFormaPag(String nomeFormaPag) {
        this.nomeFormaPag = nomeFormaPag;
    }

    public int getCodigoFormaPag() {
        return codigoFormaPag;
    }

    public void setCodigoFormaPag(int codigoFormaPag) {
        this.codigoFormaPag = codigoFormaPag;
    }
    
    
}
