/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Aluno
 */
public class Loggerr {
    
    
    private File f;
    private FileWriter fw;
    private int cont = 0;

    public Loggerr() {
        f = new File("log.txt");      
        try {            
            fw = new FileWriter(f);
            fw.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void escreve(Venda venda, Cliente c) {
        try {
            for (Filme i : venda.getItens()){
             fw.append("\r\n Cliente:"+ c.getUsuarioCliente() + " adicionou os produtos:" + venda.getItens().get(cont).getTituloFilme());
             fw.flush();//Envia os dados para o arquivo
             cont++;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        super.finalize(); 
    }
}
