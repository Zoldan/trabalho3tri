/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.model;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import vendafilme.app.JavaFXApplication5;

/**
 *
 * @author Aluno
 */
public class Cliente implements Serializable{
    
    private String nomeCliente;
    private String usuarioCliente;
    private String senha;
    private int codigoCliente;
    
    //public static Cliente clienteDaVez;

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getUsuarioCliente() {
        return usuarioCliente;
    }

    public void setUsuarioCliente(String usuarioCliente) {
        this.usuarioCliente = usuarioCliente;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
    
    
    public int inserirCliente(Cliente cliente) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO CLIENTEFILME("
                + "CODIGOCLIENTE, NOME, USUARIO, SENHA) VALUES"
                + "(SEQ_CLIENTE_CODIGOCLIENTE.nextVal,?,?,?)";

        try {

            String generatedColumns[] = {"CODIGOCLIENTE"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setString(1, cliente.getNomeCliente());
            ps.setString(2, cliente.getUsuarioCliente());
            ps.setString(3, cliente.getSenha());
            
            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into CLIENTEFILME table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            cliente.setCodigoCliente(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        
       
        return codigoCliente;
    }
    
    /**
     *
     * @param cliente
     * @return
     * @throws IOException
     */ 
    public void procurarUsuario(Cliente cliente) throws IOException {
        String selectSQL = "SELECT * FROM CLIENTEFILME WHERE usuario = ? and senha = ?";
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, cliente.getUsuarioCliente());
            ps.setString(2, cliente.getSenha());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cliente.setCodigoCliente(rs.getInt("CODIGOCLIENTE"));
                cliente.setSenha(rs.getString("SENHA"));
                cliente.setUsuarioCliente(rs.getString("USUARIO"));
                JavaFXApplication5 fx = new JavaFXApplication5();
                JavaFXApplication5.clienteDaVez = cliente;
                fx.trocaTela("menu");
            } else{
                System.out.println ("Não existe nenhum usuário com esses dados");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            
        }
    }

    
    }
