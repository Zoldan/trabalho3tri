/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Filme implements Serializable{

    private String tituloFilme;
    private int codigoFilme;
    private Double valor;
    private String diretor;
    private Double duracao;
    private int anoLancamento;
    private int codigoCategoria;
    private int quantidadeestoque;
    
   

    public int getQuantidadeestoque() {
        return quantidadeestoque;
    }

    public void setQuantidadeestoque(int quantidadeestoque) {
        this.quantidadeestoque = quantidadeestoque;
    }

    public String getTituloFilme() {
        return tituloFilme;
    }

    public void setTituloFilme(String tituloFilme) {
        this.tituloFilme = tituloFilme;
    }

    public int getCodigoFilme() {
        return codigoFilme;
    }

    public void setCodigoFilme(int codigoFilme) {
        this.codigoFilme = codigoFilme;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getDiretor() {
        return diretor;
    }

    public void setDiretor(String diretor) {
        this.diretor = diretor;
    }

    public Double getDuracao() {
        return duracao;
    }

    public void setDuracao(Double duracao) {
        this.duracao = duracao;
    }

    public int getAnoLancamento() {
        return anoLancamento;
    }

    public void setAnoLancamento(int anoLancamento) {
        this.anoLancamento = anoLancamento;
    }

    public int getCodigoCategoria() {
        return codigoCategoria;
    }

    public void setCodigoCategoria(int codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    public ArrayList<Filme> getAll() throws SQLException {

        String selectSQL = "select * from Filme"; //where id> bla bla
        ArrayList<Filme> listaFilme = new ArrayList<>();
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        Statement st;
        try {

            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);

            while (rs.next()) {
                Filme f = new Filme();
                f.setCodigoFilme(rs.getInt("CODIGO_FILME"));
                f.setTituloFilme(rs.getString("TITULO"));
                f.setDiretor(rs.getString("DIRETOR"));
                f.setDuracao(rs.getDouble("DURACAO"));
                f.setAnoLancamento(rs.getInt("ANOLANCAMENTO"));
                f.setValor(rs.getDouble("VALOR"));
                f.setQuantidadeestoque(rs.getInt("QUANTIDADEESTOQUE"));
                f.setCodigoCategoria(rs.getInt("CODIGOCATG"));
                listaFilme.add(f);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conexao.desconecta();

        }
        return listaFilme;
    }

    public void atualizarFilme() { // ok

        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement prepareStatement = null;

        String updateTableSQL = "update Filme set TITULO = (?), VALOR = (?), DIRETOR = (?), DURACAO = (?), ANOLANCAMENTO =(?),"
                + "CODIGOCATG = (?), QUANTIDADEESTOQUE = (?) where CODIGO_FILME = ?";
        // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            prepareStatement = dbConnection.prepareStatement(updateTableSQL);
            prepareStatement.setString(1, this.getTituloFilme());
            prepareStatement.setDouble(2, this.getValor());
            prepareStatement.setString(3, this.getDiretor());
            prepareStatement.setDouble(4, this.getDuracao());
            prepareStatement.setInt(5, this.getAnoLancamento());
            prepareStatement.setInt(6, this.getCodigoCategoria());
            prepareStatement.setInt(7, this.getQuantidadeestoque());
            prepareStatement.setInt(8, this.getCodigoFilme());
            
            //execute insert SQL Statement
            prepareStatement.executeUpdate();

            System.out.println("Filme modificado!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
