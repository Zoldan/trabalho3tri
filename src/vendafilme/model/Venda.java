/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import vendafilme.app.CatalogoController;

/**
 *
 * @author Aluno
 */
public class Venda implements Serializable{

    private int codigoVenda;
    private int codigoUsuario;
    private int codigoFormaPagamento;
    private ArrayList<Filme> itens = new ArrayList<>();
    private Double cont =0.0;

    public void adicionaArrayList(Filme f) {
        itens.add(f);
    }

    public ArrayList<Filme> getItens() {
        return itens;
    }

    public void setItens(ArrayList<Filme> itens) {
        this.itens = itens;
    }
    
    

    public int getCodigoVenda() {
        return codigoVenda;
    }

    public void setCodigoVenda(int codigoVenda) {
        this.codigoVenda = codigoVenda;
    }

    public int getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(int codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public int getCodigoFormaPagamento() {
        return codigoFormaPagamento;
    }

    public void setCodigoFormaPagamento(int codigoFormaPagamento) {
        this.codigoFormaPagamento = codigoFormaPagamento;
    }

    public Double somaTotal() {
        
        for (Filme f : itens) {
            cont = f.getValor() + cont;
        }
        return cont;
    }

    public int inserirVenda(Venda venda) throws SQLException {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO VENDA("
                + "CODIGO_VENDA, CODIGOCLIENTE, COD_FORMAPAG) VALUES"
                + "(SEQ_VENDA_CODIGO_VENDA.nextVal,?,?)";

        try {

            String generatedColumns[] = {"CODIGO_VENDA"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();

            ps.setInt(1, venda.getCodigoUsuario());
            ps.setInt(2, venda.getCodigoFormaPagamento());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into VENDA table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            venda.setCodigoVenda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        /// for{
        String insertTableSQL2 = "INSERT INTO FILME_VENDA (CODIGO_VENDA, CODIGO_FILME, PRECO) VALUES (?,?,?)";
        try {

            ps = dbConnection.prepareStatement(insertTableSQL2);

            st = dbConnection.createStatement();

            ps.setInt(1, venda.getCodigoVenda());
            ps.setInt(2, CatalogoController.f.getCodigoFilme());
            ps.setDouble(3, CatalogoController.f.getValor());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into FILMEVENDA table!");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        //   }
        return codigoVenda;

    }

    public void inserirVenda() {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int codigoVenda = 0;

        String insertTableSQL = "INSERT INTO VENDA("
                + "CODIGO_VENDA, CODIGOCLIENTE, COD_FORMAPAG) VALUES"
                + "(SEQ_VENDA_CODIGO_VENDA.nextVal,?,?)";

        try {

            String generatedColumns[] = {"CODIGO_VENDA"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();

            ps.setInt(1, this.getCodigoUsuario());
            ps.setInt(2, this.getCodigoFormaPagamento());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into VENDA table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            this.setCodigoVenda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        String insertTableSQL2 = "INSERT INTO FILME_VENDA (CODIGO_VENDA, CODIGO_FILME, PRECO, quantidade) VALUES (?,?,?,?)";
        try {
            Double pegaTotal;
            for (Filme f : itens) {

                pegaTotal = this.somaTotal();

                ps = dbConnection.prepareStatement(insertTableSQL2);

                st = dbConnection.createStatement();

                ps.setInt(1, this.getCodigoVenda());
                ps.setInt(2, f.getCodigoFilme());
                ps.setDouble(3, pegaTotal);
                ps.setInt(4, f.getQuantidadeestoque());
                f.setQuantidadeestoque(f.getQuantidadeestoque() - 1);
                f.atualizarFilme();

                //execute insert SQL statement
                ps.executeUpdate();

                //ver qual codigo da tua venda
                System.out.println("Record is inserted into FILMEVENDA table!");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
