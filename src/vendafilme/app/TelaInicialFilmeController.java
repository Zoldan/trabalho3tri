/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class TelaInicialFilmeController implements Initializable {

    /**
     * Initializes the controller class.
     */
    public static int cont=0;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void selecionaCadastrar() throws IOException{
        JavaFXApplication5 fx = new JavaFXApplication5();
         TelaInicialFilmeController.cont =1;
        fx.trocaTela("telaCadastroLogin");
      
        
    }
    public void selecionaLogar() throws IOException{
        JavaFXApplication5 fx = new JavaFXApplication5();
        TelaInicialFilmeController.cont =2;
        fx.trocaTela("telaCadastroLogin");
        
        
    }
}
