/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.app;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import static vendafilme.app.CatalogoController.venda;
import vendafilme.model.Cliente;
import vendafilme.model.Filme;
import vendafilme.model.Venda;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class CompraVariosController implements Initializable {

    private Button finalizarCompraButton;
    @FXML
    private Label totalL;

    @FXML
    private RadioButton dinheiro;
    @FXML
    private RadioButton debito;
    @FXML
    private RadioButton credito;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        totalL.setText(Double.toString(CatalogoController.venda.somaTotal()));
    }

    public void comprarFilmesCarrinho() {
  
        Venda vendaLocal = CatalogoController.venda;

        if (dinheiro.isSelected()) {

            vendaLocal.setCodigoFormaPagamento(1);

        } else if (debito.isSelected()) {

            vendaLocal.setCodigoFormaPagamento(2);

        } else if (credito.isSelected()) {

            vendaLocal.setCodigoFormaPagamento(3);

        }
        //vendaLocal.setCodigoUsuario(Cliente.clienteDaVez.getCodigoCliente());
        vendaLocal.setCodigoUsuario(JavaFXApplication5.clienteDaVez.getCodigoCliente());

        vendaLocal.inserirVenda();
        
        CatalogoController.venda.getItens().clear();
        
        try{
         
        /*
         * A Classe FileOutputStream é responsável por criar
         * o arquivo fisicamente no disco, assim poderemos realizar a 
         * escrita neste. 
         * */
        FileOutputStream fout = new FileOutputStream("usuario."+ JavaFXApplication5.clienteDaVez.getCodigoCliente());
         
        /*
         * A Classe ObjectOutputStream escreve os objetos no FileOutputStream
         * */
        ObjectOutputStream oos = new ObjectOutputStream(fout);   
         
        /*
         * Veja aqui a mágica ocorrendo: Estamos gravando um objeto 
         * do tipo Address no arquivo address.ser. Atenção: O nosso 
         * objeto Address que está sendo gravado, já é gravado de forma 
         * serializada
         * */
        oos.writeObject(venda);
         
        oos.close();
        System.out.println("Done");
  
       }catch(Exception ex){
           ex.printStackTrace();
       } 

    }
}
