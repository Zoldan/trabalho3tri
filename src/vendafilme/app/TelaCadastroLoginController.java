/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import vendafilme.model.Cliente;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class TelaCadastroLoginController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Text textoL;
    
    @FXML
    private Text textoC;
    
    @FXML
    private Button cadastrar;
    
    @FXML
    private Button logar;
    
    @FXML
    private TextField nomeTF;
    
    @FXML
    private TextField usuarioTF;
    
    @FXML
    private TextField senhaTF;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        System.out.println(TelaInicialFilmeController.cont);
        if (TelaInicialFilmeController.cont == 1)  {
            textoL.setVisible(false);
            logar.setVisible(false);
            
        }else if (TelaInicialFilmeController.cont == 2) {
            textoC.setVisible(false);
            cadastrar.setVisible(false);
            
        }    
    
    }
    
    public void cadastrarCliente (){
        
        Cliente c = new Cliente();
        c.setNomeCliente(nomeTF.getText());
        c.setUsuarioCliente(usuarioTF.getText());
        c.setSenha(senhaTF.getText());
        c.inserirCliente(c);
    }
    
   public void logarCliente (){
       
        Cliente c = new Cliente();
        c.setUsuarioCliente(usuarioTF.getText());
        c.setSenha(senhaTF.getText());
        try {
            c.procurarUsuario(c);
            JavaFXApplication5.clienteDaVez = c;
        } catch (IOException ex) {
            Logger.getLogger(TelaCadastroLoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(JavaFXApplication5.clienteDaVez.getUsuarioCliente());
   }
   
}
