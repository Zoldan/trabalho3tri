/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.app;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import vendafilme.model.Cliente;
import vendafilme.model.Filme;
import vendafilme.model.Venda;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class Compra1soController implements Initializable {

    @FXML
    private Label nomeDoFilmeLabel;
    @FXML
    private Label maximoLabel;
    @FXML
    private Button finalizarCompraButton;
    @FXML
    private Label totalFilmeLabel;
    @FXML
    private Label valorDoFilmeLabel;
    @FXML
    private RadioButton dinheiro;
    @FXML
    private RadioButton debito;
    @FXML
    private RadioButton credito;
    private TextField quantidade;
    
    public static int passaQuantidade;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomeDoFilmeLabel.setText(CatalogoController.f.getTituloFilme());
        maximoLabel.setText(Integer.toString(CatalogoController.f.getQuantidadeestoque()));
        totalFilmeLabel.setText(Double.toString(CatalogoController.f.getValor()));
        valorDoFilmeLabel.setText(Double.toString(CatalogoController.f.getValor()));

    }

    @FXML
    public void finalizarCompraItem() {

        Venda venda = new Venda();

        if (dinheiro.isSelected()) {

            venda.setCodigoFormaPagamento(1);

        } else if (debito.isSelected()) {

            venda.setCodigoFormaPagamento(2);

        } else if (credito.isSelected()) {

            venda.setCodigoFormaPagamento(3);

        }

        //venda.setCodigoUsuario(Cliente.clienteDaVez.getCodigoCliente());
        venda.setCodigoUsuario(JavaFXApplication5.clienteDaVez.getCodigoCliente());
        
        try {
            venda.inserirVenda(venda);
            
        } catch (SQLException ex) {
            Logger.getLogger(Compra1soController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        passaQuantidade = CatalogoController.f.getQuantidadeestoque() - Integer.parseInt(quantidade.getText());
        CatalogoController.f.setQuantidadeestoque(passaQuantidade);
        CatalogoController.f.atualizarFilme();
    }

}
