/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendafilme.app;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import vendafilme.model.Carrinho;
import vendafilme.model.Cliente;
import vendafilme.model.Filme;
import vendafilme.model.Loggerr;
import vendafilme.model.Venda;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class CatalogoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TableView<Filme> catalogoTableView;
    @FXML
    private TableView<Filme> carrinhoTableView;
    @FXML
    private TableColumn<Filme, String> Titulo;
    @FXML
    private TableColumn<Filme, String> Diretor;
    @FXML
    private TableColumn<Filme, Double> Duracao;
    @FXML
    private TableColumn<Filme, Integer> AnoLancamento;
    @FXML
    private TableColumn<Filme, String> Categoria;
    @FXML
    private TableColumn<Filme, Integer> Qtd;
    @FXML
    private TableColumn<Filme, Double> ValorCatalogo;
    @FXML
    private TableColumn<Filme, Integer> Cdg;
    @FXML
    private TableColumn<Filme, String> Nome;
    @FXML
    private TableColumn<Filme, Double> Valor;
    @FXML
    private TableColumn<Filme, Double> Total;
    @FXML
    static public ObservableList<Filme> obFilmeCatalogo;
    @FXML
    static public ObservableList<Filme> obCarrinho;

    public static Filme f = new Filme();
    public static Venda venda = new Venda();
    private String str = "";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
      
        obFilmeCatalogo = catalogoTableView.getItems();
        obCarrinho = carrinhoTableView.getItems();
        try {
              Nome.setCellValueFactory(new PropertyValueFactory<>("tituloFilme"));
              Valor.setCellValueFactory(new PropertyValueFactory<>("valor"));
                adicionaFilme();

            Titulo.setCellValueFactory(new PropertyValueFactory<>("tituloFilme"));
            Diretor.setCellValueFactory(new PropertyValueFactory<>("Diretor"));
            Duracao.setCellValueFactory(new PropertyValueFactory<>("Duracao"));
            AnoLancamento.setCellValueFactory(new PropertyValueFactory<>("AnoLancamento"));
            Categoria.setCellValueFactory(new PropertyValueFactory<>("codigoCategoria"));
            Qtd.setCellValueFactory(new PropertyValueFactory<>("quantidadeestoque"));
            ValorCatalogo.setCellValueFactory(new PropertyValueFactory<>("valor"));
            Cdg.setCellValueFactory(new PropertyValueFactory<>("codigoFilme"));

        } catch (SQLException ex) {
            Logger.getLogger(CatalogoController.class.getName()).log(Level.SEVERE, null, ex);

        }

        this.catalogoTableView.setItems(obFilmeCatalogo);
          try{
            
           /*
            * Responsável por carregar o arquivo address.ser
            * */
           FileInputStream fin = new FileInputStream("usuario."+ JavaFXApplication5.clienteDaVez.getCodigoCliente());
            
           /*
            * Responsável por ler o objeto referente ao arquivo
            * */
           ObjectInputStream ois = new ObjectInputStream(fin);
            
            /*
             * Aqui a mágica é feita, onde os bytes presentes no arquivo address.ser
             * são convertidos em uma instância de Address.
             * */
            venda = (Venda) ois.readObject();
            
           ois.close();
  
           for (Filme f : venda.getItens()){
               
               obCarrinho.add(f);
               
           }
           
           System.out.println(venda);
  
       }catch(Exception ex){
           ex.printStackTrace(); 
       } 
    }

    private void adicionaFilme() throws SQLException {
        Filme filme = new Filme();
        ArrayList<Filme> listaBanco = new ArrayList<>();
        for (Filme f : filme.getAll()) {

            listaBanco.add(f);

        }
        int i = 0;
        ///Copiar os valores de um vetor para outro
        for (i = 0; i < listaBanco.size(); i++) {
            obFilmeCatalogo.add(listaBanco.get(i));
        }
    }

    public void adicionaCarrinho() {

        Filme selectedItem = catalogoTableView.getSelectionModel().getSelectedItem();
        obCarrinho.add(selectedItem);
        venda.adicionaArrayList(selectedItem);
        System.out.println(selectedItem.getTituloFilme());
        Nome.setCellValueFactory(new PropertyValueFactory<>("tituloFilme"));
        Valor.setCellValueFactory(new PropertyValueFactory<>("valor"));
        //  Total.setCellValueFactory(new PropertyValueFactory<>("total"));
        carrinhoTableView.setItems(obCarrinho);

        str = selectedItem.getTituloFilme();
        Loggerr log = new Loggerr();
        
        log.escreve(venda, JavaFXApplication5.clienteDaVez);
        try{
         
        /*
         * A Classe FileOutputStream é responsável por criar
         * o arquivo fisicamente no disco, assim poderemos realizar a 
         * escrita neste. 
         * */
        FileOutputStream fout = new FileOutputStream("usuario."+ JavaFXApplication5.clienteDaVez.getCodigoCliente());
         
        /*
         * A Classe ObjectOutputStream escreve os objetos no FileOutputStream
         * */
        ObjectOutputStream oos = new ObjectOutputStream(fout);   
         
        /*
         * Veja aqui a mágica ocorrendo: Estamos gravando um objeto 
         * do tipo Address no arquivo address.ser. Atenção: O nosso 
         * objeto Address que está sendo gravado, já é gravado de forma 
         * serializada
         * */
        oos.writeObject(venda);
         
        oos.close();
        System.out.println("Done");
  
       }catch(Exception ex){
           ex.printStackTrace();
       } 
    }

    public void comprarFilme() throws IOException {

        Filme selectedItem = catalogoTableView.getSelectionModel().getSelectedItem();

        f = selectedItem;

        JavaFXApplication5 fx = new JavaFXApplication5();
        fx.trocaTela("compra1so");
    }

    public void finalizarCompra() throws IOException{
    
        JavaFXApplication5 fx = new JavaFXApplication5();
        fx.trocaTela("compravarios");
        
        
        
        
        
    }
}
